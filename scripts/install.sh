#!/bin/sh

echo "copiando binarios..."

cd ../bin
sudo cp jopad /usr/bin/

echo "Creando directorios..."

sudo mkdir -p /usr/share/jopad
sudo cp -r ../resources /usr/share/jopad/

echo "Creando lanzador"

cat > jopad.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false
Exec=jopad
Name=Jopad
Categories=Qt;Utility;TextEditor;
Comment=Edit text files 
Icon=/usr/share/jopad/resources/icon.png
Keywords=text;editor;files;notepad;
EOF

echo "instalando entrada de lanzador..."

sudo desktop-file-install jopad.desktop

echo "removiendo archivos de instalación..."

rm jopad.desktop

echo "Instalación completada con éxito."

