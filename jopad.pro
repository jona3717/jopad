QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

SOURCES += \
    src/main.cpp \
    src/jopad.cpp\
    src/aboutjopad.cpp\

HEADERS += \
    src/jopad.h\
    src/aboutjopad.h\

FORMS += \
    src/jopad.ui\
    src/aboutjopad.ui\

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
