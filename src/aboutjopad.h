#ifndef ABOUTJOPAD_H
#define ABOUTJOPAD_H

#include <QDialog>

namespace Ui {
class AboutJopad;
}

class AboutJopad : public QDialog
{
    Q_OBJECT

public:
    explicit AboutJopad(QWidget *parent = nullptr);
    ~AboutJopad();

private slots:
    void on_btnMail_clicked();

private:
    Ui::AboutJopad *ui;
};

#endif // ABOUTJOPAD_H
