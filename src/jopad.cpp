#include "jopad.h"
#include "./ui_jopad.h"

#include "aboutjopad.h"
#include "ui_aboutjopad.h"

#include "QProcess"
#include "QFileDialog"
#include "QFile"
#include "QMessageBox"
#include "QTextStream"
#include "QResizeEvent"

Jopad::Jopad(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Jopad)
{
    ui->setupUi(this);
    qApp->installEventFilter(this);

    QIcon newIcon("/usr/share/jopad/resources/new.png");
    QIcon openIcon("/usr/share/jopad/resources/open.png");
    QIcon saveIcon("/usr/share/jopad/resources/save.png");
    QIcon saveAsIcon("/usr/share/jopad/resources/save_as.png");
    QIcon undoIcon("/usr/share/jopad/resources/undo.png");
    QIcon redoIcon("/usr/share/jopad/resources/redo.png");
    QIcon helpIcon("/usr/share/jopad/resources/help.png");
    QIcon copyIcon("/usr/share/jopad/resources/copy.png");
    QIcon cutIcon("/usr/share/jopad/resources/cut.png");
    QIcon pasteIcon("/usr/share/jopad/resources/paste.png");
    QIcon selectAllIcon("/usr/share/jopad/resources/select_all.png");
    QIcon searchIcon("/usr/share/jopad/resources/search.png");
    QIcon replaceIcon("/usr/share/jopad/resources/replace.png");

    QIcon closeSearch("/usr/share/jopad/resources/exit.png");

    ui->actionNew->setIcon(newIcon);
    ui->actionOpen->setIcon(openIcon);
    ui->actionSave->setIcon(saveIcon);
    ui->actionSaveAs->setIcon(saveAsIcon);
    ui->actionUndo->setIcon(undoIcon);
    ui->actionRedo->setIcon(redoIcon);
    ui->actionJopad->setIcon(helpIcon);
    ui->actionCopy->setIcon(copyIcon);
    ui->actionCut->setIcon(cutIcon);
    ui->actionPaste->setIcon(pasteIcon);
    ui->actionSelectAll->setIcon(selectAllIcon);
    ui->actionSearch->setIcon(searchIcon);
    ui->actionReplace->setIcon(replaceIcon);

    ui->btnCloseSearch->setIcon(closeSearch);

    ui->txtSearch->hide();
    ui->btnBack->hide();
    ui->btnNext->hide();
    ui->btnCloseSearch->hide();
    ui->btnSearch->hide();
    ui->txtReplace->hide();
    ui->btnReplace->hide();
    ui->btnReplaceAll->hide();
}

Jopad::~Jopad()
{
    delete ui;
}

bool Jopad::confirmation(){
    bool value = true;
    newText = ui->txtSheet->toPlainText();
    QMessageBox::StandardButton reply;

    if ((!filePath.isEmpty() && actualText != newText) || (filePath.isEmpty() && !newText.isEmpty())){
        reply = QMessageBox::question(this, "Confirm", "Save changes to document before closing?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
        if (reply == QMessageBox::Yes){
            Jopad::on_actionSave_triggered();
            value = true;
        }
        else if (reply == QMessageBox::No){
            filePath = "null";
            value = true;
        }
        else
            value = false;
    }else
        filePath = "null";
    return value;
}

void Jopad::save(QString path){
    QFile file(path);
    if(file.open(QIODevice::WriteOnly | QFile::Text)){
        QTextStream out(&file);
        out << ui->txtSheet->toPlainText();
        file.flush();
        file.close();
        actualText=ui->txtSheet->toPlainText();
        ui->statusBar->showMessage("Documento guardado exitosamente.");
    }
    else
        QMessageBox::warning(this, "Error", "No se ha podido guardar el documento.");
}

void Jopad::open(QString path){
    if(path != "null"){
        QFile file(path);

        if (file.open(QIODevice::ReadOnly | QFile::Text)){
            statusBar()->showMessage(file.fileName());
            QTextStream in(&file);
            QString text = in.readAll();
            ui->txtSheet->setPlainText(text);
            actualText = text;
            file.close();
            ui->txtSheet->moveCursor(QTextCursor::Start);
        }
        else{
            QMessageBox::warning(this, "Error", "No se pudo abrir el archivo.");
        }
    }

}

bool Jopad::search(QTextDocument::FindFlags option){
    QString word = ui->txtSearch->text();
    bool value = true;

    if (!word.isEmpty()){
        if (!(ui->txtSheet->find(word, option)))
            value =false;
    }
    else {
        QMessageBox::warning(this, "Error", "Debes colocar una palabra en el cuado de búsqueda.");
    }

    return value;
}

void Jopad::closeEvent(QCloseEvent *event){
    bool value = Jopad::confirmation();

    if (!value || (value && filePath.isEmpty()))
        event->ignore();
    else
        event->accept();

}

void Jopad::on_actionNew_triggered(){
    QProcess *process;
    process = new QProcess();
    process->setProgram("jopad");
    process->start();
}

void Jopad::on_actionOpen_triggered(){
    if (Jopad::confirmation()){
        filePath = QFileDialog::getOpenFileName(this, tr("Abrir archivo"),"~/");

        if (!filePath.isEmpty() || !filePath.isNull()) {
            Jopad::open(filePath);
        }
    }
    else
        return;
}

void Jopad::on_actionSave_triggered(){
    if (!filePath.isEmpty() || !filePath.isNull()){
        Jopad::save(filePath);
    }
    else
        on_actionSaveAs_triggered();
}

void Jopad::on_actionSaveAs_triggered(){
    QString path = QFileDialog::getSaveFileName(this, tr("Guardar archivo"), "~/");
    if (!path.isEmpty()){
        Jopad::save(path);
        actualText = ui->txtSheet->toPlainText();
        filePath = path;
    }
}

void Jopad::on_actionJopad_triggered(){
    AboutJopad d;
    QPixmap pixmap("/usr/share/jopad/resources/icon.png");
    d.setWindowIcon(pixmap);
    d.exec();
}

void Jopad::on_actionUndo_triggered(){
    ui->txtSheet->undo();

}

void Jopad::on_actionRedo_triggered(){
    ui->txtSheet->redo();
}

void Jopad::on_actionSelectAll_triggered(){
    ui->txtSheet->selectAll();
}

void Jopad::on_actionCopy_triggered(){
    ui->txtSheet->copy();
}

void Jopad::on_actionCut_triggered(){
    ui->txtSheet->cut();
}

void Jopad::on_actionPaste_triggered(){
    ui->txtSheet->paste();
}


void Jopad::on_actionSearch_triggered(){
    if (ui->txtSearch->isHidden()){
        ui->txtSearch->show();
        ui->btnSearch->show();
        ui->btnBack->show();
        ui->btnNext->show();
        ui->btnCloseSearch->show();

        ui->txtSearch->setFocus();
    }
    else {
        ui->txtSearch->hide();
        ui->btnSearch->hide();
        ui->btnBack->hide();
        ui->btnNext->hide();
        ui->btnCloseSearch->hide();

        ui->txtReplace->hide();
        ui->btnReplace->hide();
        ui->btnReplaceAll->hide();
    }
}

void Jopad::on_actionReplace_triggered(){
    if (ui->txtReplace->isHidden()) {
        ui->txtSearch->show();
        ui->btnSearch->show();
        ui->btnBack->show();
        ui->btnNext->show();
        ui->btnCloseSearch->show();

        ui->txtReplace->show();
        ui->btnReplace->show();
        ui->btnReplaceAll->show();

        ui->txtReplace->setFocus();
    }
    else {
        ui->txtSearch->hide();
        ui->btnSearch->hide();
        ui->btnBack->hide();
        ui->btnNext->hide();
        ui->btnCloseSearch->hide();

        ui->txtReplace->hide();
        ui->btnReplace->hide();
        ui->btnReplaceAll->hide();
    }
}

void Jopad::on_btnSearch_clicked(){
    ui->txtSheet->moveCursor(QTextCursor::Start);
    if (!Jopad::search(QTextDocument::FindWholeWords)) {
        ui->statusBar->showMessage("No hay coincidencias. Pulse en nuevamente para buscar desde el principio.");
    }
}

void Jopad::on_btnBack_clicked(){
    if (!Jopad::search(QTextDocument::FindBackward)){
        ui->statusBar->showMessage("Ha llegado al principio del documento.");
    }
}

void Jopad::on_btnNext_clicked(){
    if (!Jopad::search(QTextDocument::FindWholeWords)) {
        ui->statusBar->showMessage("Ha llegado al final del documento.");
    }
}

void Jopad::on_btnReplace_clicked(){
    QString searchedWord = ui->txtSearch->text();
    auto replacementWord = ui->txtReplace->text();
    if (!replacementWord.isEmpty() && !searchedWord.isEmpty()){
        ui->txtSheet->moveCursor(QTextCursor::WordLeft);
        Jopad::search(QTextDocument::FindWholeWords);
        if (ui->txtSheet->textCursor().selectedText() == ui->txtSearch->text()){
            ui->txtSheet->textCursor().insertText(replacementWord);
            ui->txtSheet->textCursor().select(QTextCursor::WordUnderCursor);
            ui->statusBar->showMessage("");
        }
        else{
            ui->statusBar->showMessage("No hay coincidencias. Pulse en nuevamente para buscar desde el principio.");
        }
    }
    else
        QMessageBox::warning(this, "Error", "Casilla en blanco");
}

void Jopad::on_btnReplaceAll_clicked(){
    bool search = true;

    do {
        Jopad::on_btnReplace_clicked();
        ui->txtSheet->moveCursor(QTextCursor::Start);
        search = Jopad::search(QTextDocument::FindWholeWords);
    } while (search);

}

void Jopad::on_btnCloseSearch_clicked(){
    Jopad::on_actionSearch_triggered();
}

void Jopad::on_actionClose_triggered(){
    close();
}

void Jopad::on_actionBold_triggered(){
    Jopad::setFormat();
}

void Jopad::on_actionItalic_triggered(){
    Jopad::setFormat();
}

void Jopad::on_actionUnderline_triggered(){
    Jopad::setFormat();
}

void Jopad::setFormat(){
    QString text = ui->txtSheet->toPlainText();
    ui->txtSheet->setText(text);
    ui->txtSheet->moveCursor(QTextCursor::End);

    bool bold = ui->actionBold->isChecked();
    bool italic = ui->actionItalic->isChecked();
    bool underline = ui->actionUnderline->isChecked();
    
    ui->txtSheet->selectAll();

    if ( bold )
        ui->txtSheet->setFontWeight(75);
    else
        ui->txtSheet->setFontWeight(50);

    ui->txtSheet->setFontItalic(italic);
    ui->txtSheet->setFontUnderline(underline);
}

bool Jopad::eventFilter(QObject* object, QEvent* event){
    if((object == ui->txtSearch || object == ui->txtReplace) && event->type() == QEvent::KeyPress){
        QKeyEvent *key = static_cast<QKeyEvent *>(event);
        if(key->key() == Qt::Key_Escape){
            ui->btnCloseSearch->animateClick();
        }
    }
    return QObject::eventFilter(object, event);
}
