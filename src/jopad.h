#ifndef JOPAD_H
#define JOPAD_H

#include <QMainWindow>
#include "QMessageBox"
#include "QTextDocument"

QT_BEGIN_NAMESPACE
namespace Ui { class Jopad; }
QT_END_NAMESPACE

class Jopad : public QMainWindow
{
    Q_OBJECT

protected:
    void closeEvent(QCloseEvent *event);

public:
    Jopad(QWidget *parent = nullptr);
    bool confirmation();
    bool search(QTextDocument::FindFlags flags);
    void save(QString);
    void open(QString);
    void setFormat();
    QString filePath;
    ~Jopad();

private slots:
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSaveAs_triggered();
    void on_actionJopad_triggered();
    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionCopy_triggered();
    void on_actionCut_triggered();
    void on_actionPaste_triggered();
    void on_actionSelectAll_triggered();
    void on_actionSearch_triggered();
    void on_actionReplace_triggered();
    void on_actionBold_triggered();
    void on_actionItalic_triggered();
    void on_actionUnderline_triggered();
    void on_actionClose_triggered();

    void on_btnSearch_clicked();
    void on_btnReplace_clicked();
    void on_btnBack_clicked();
    void on_btnNext_clicked();
    void on_btnCloseSearch_clicked();
    void on_btnReplaceAll_clicked();

private:
    Ui::Jopad *ui;
    QString actualText, newText;

    bool eventFilter(QObject *object, QEvent *event);
};
#endif // JOPAD_H
