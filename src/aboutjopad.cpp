#include "aboutjopad.h"
#include "ui_aboutjopad.h"
#include "QBitmap"
#include "QTextFormat"
#include "QDesktopServices"
#include "QUrl"

AboutJopad::AboutJopad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutJopad)
{
    ui->setupUi(this);
    QPixmap pixmap("/usr/share/jopad/resources/icon.png");
    ui->lblIcon->setPixmap(pixmap);
    ui->lblIcon->show();
    ui->lblVersion->setText("<b>Versión 1.2</b>");
}

AboutJopad::~AboutJopad()
{
    delete ui;
}

void AboutJopad::on_btnMail_clicked(){
    QDesktopServices::openUrl(QUrl("mailto:jona3717@protonmail.com"));
}
