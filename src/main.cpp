#include "jopad.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Jopad w;
    QPixmap pixmap("/usr/share/jopad/resources/icon.png");
    w.setWindowIcon(pixmap);
    w.show();

    if (argc > 1){
        w.open(argv[1]);
        w.filePath = argv[1];
    }
    return a.exec();
}
